import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {AnimalCardComponent} from "./animal/components/animal-card/animal-card.component";
import { AnimalPageComponent } from './animal/components/animal-page/animal-page.component';
import {AppRoutingModule} from "./app.router.module";
import { HttpClientModule } from "@angular/common/http";
import { VisitorPageComponent } from './visitor/components/visitor-page/visitor-page.component';
import { SponsorAnimalComponent } from './visitor/components/sponsor-animal/sponsor-animal.component';
import {FormsModule} from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [ // Expose component here
    AppComponent,
    AnimalCardComponent,
    AnimalPageComponent,
    VisitorPageComponent,
    SponsorAnimalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
