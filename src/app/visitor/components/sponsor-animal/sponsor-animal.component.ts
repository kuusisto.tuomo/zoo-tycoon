import { Component, OnInit } from '@angular/core';
import {NgForm} from "@angular/forms";
import {OwnedAnimalsService} from "../../../animal/services/owned-animals.service";
import {Animal} from "../../../animal/models/animal.model";

@Component({
  selector: 'app-sponsor-animal',
  templateUrl: './sponsor-animal.component.html',
  styleUrls: ['./sponsor-animal.component.css']
})
export class SponsorAnimalComponent implements OnInit {

  constructor(private readonly ownedAnimals: OwnedAnimalsService) { }

  ngOnInit(): void {
  }

  get owned() : Animal[] {
    return this.ownedAnimals.owned;
  }

  public onSubmit(form: NgForm) : void{
    // console.log("form.value", form.value);
    let { amount, id } = form.value;
    if(this.owned.length > 0){
      amount = Math.abs(amount);
      this.ownedAnimals.update_value( id, amount );
    }else{
      console.log("Could not find an animal to donate to")
    }
  }
}
