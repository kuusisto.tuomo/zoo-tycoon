import { Component } from '@angular/core';
import keycloak from "../keycloak";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Zoo Tycoon';

  handleLoginKeycloak (): void {
    keycloak.login();
  }

  handleLogoutKeycloak (): void {
    keycloak.logout();
  }

  getKeycloakToken(): string | undefined {
    return keycloak.token;
  }

  username(): string | undefined {
    return keycloak.tokenParsed?.given_name;
  }

  get loggedIn() : boolean | undefined {
    return keycloak.authenticated;
  }
}
