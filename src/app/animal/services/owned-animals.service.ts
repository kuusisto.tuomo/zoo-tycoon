import { Injectable } from '@angular/core';
import {Animal} from "../models/animal.model";
import {FetchPurchasableService} from "./fetch-purchasable.service";

@Injectable({
  providedIn: 'root'
})
export class OwnedAnimalsService {

  private _owned: Animal[] = [];

  get owned(): Animal[]{
    return this._owned;
  }

  public add_owned( new_animal : Animal ) : void {
    // Ensure animal is unique before it gets added
    if(this._owned.filter( item => item.id == new_animal.id ).length === 0){
      this._owned.push( new_animal );
    }
    else{
      alert(`You already own a ${new_animal.name.toLowerCase()}!`);
    }
  }

  public update_value( id : number, value : number ){
    this._owned.filter( item => item.id == id)[0].value += value;
  }

  constructor( private readonly fetchPurchasable : FetchPurchasableService ) {
    this.fetchPurchasable.getNewPurchasable(5).subscribe(
      (animals) => { animals.forEach(a => this._owned.push(a)) },
      (error) => { console.error("Could not add starting animal(s)", error) }
    );
  }
}
