import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {map, Observable, tap} from "rxjs";
import { Raw } from "../models/raw.model";
import {Animal} from "../models/animal.model";

@Injectable({
  providedIn: 'root'
})
export class FetchPurchasableService {

  constructor(private readonly  http: HttpClient) { }

  getNewPurchasable (count: number = 1) : Observable<Animal[]> {
    return this.http.get<Raw[]>(`https://zoo-animal-api.herokuapp.com/animals/rand/${count}`)
      .pipe(
        tap( item => console.log(item) ),
        map( raws => {
          return raws.map( item => ({
            name: item.name,
            picture: item.image_link,
            id: item.id,
            value: Array.from(item.name).map(letter => letter.charCodeAt(0)).reduce((a,b) => a + b, 0)
          }))
        })
      )
  }

  debug_test () : string {
    return "Hello from FetchPurchasableService";
  }
}
