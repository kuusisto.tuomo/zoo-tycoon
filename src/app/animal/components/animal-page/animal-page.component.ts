import { Component, OnInit } from '@angular/core';
import {Animal} from "../../models/animal.model";
import {FetchPurchasableService} from "../../services/fetch-purchasable.service";
import {Raw} from "../../models/raw.model";
import {OwnedAnimalsService} from "../../services/owned-animals.service";

@Component({
  selector: 'app-animal-page',
  templateUrl: './animal-page.component.html',
  styleUrls: ['./animal-page.component.css']
})
export class AnimalPageComponent implements OnInit {

  constructor(
    private readonly fetchPurchasable : FetchPurchasableService,
    private readonly ownedAnimals: OwnedAnimalsService
  ){}

  get owned() : Animal[] {
    return this.ownedAnimals.owned;
  }

  ngOnInit(): void {
    console.log("app-animal-page", this.fetchPurchasable.debug_test());
    const response = this.fetchPurchasable.getNewPurchasable(5)
      .subscribe( // success => {}, error => {}
        (animals: Animal[]) => {
          animals.map( item => this.purchasable.push( item ) );
        },
        (error : any) => {
          console.error("Problem fetching new animals", error)
        }
      );
    console.log("response", response);
  }

  handleSelectedEvent (id:number) : void{
    console.log("Animal purchased: " + id);
    // this.owned.push( this.purchasable.filter(a => a.id == id)[0] );
    const purchased_animal = this.purchasable.filter(a => a.id == id)[0];
    this.ownedAnimals.add_owned( purchased_animal );
    this.purchasable = this.purchasable.filter(a => a.id != id);
  }
  // public owned: Animal[] = [
  //   {
  //     name: "Blue Tree Monitor",
  //     picture:"https://upload.wikimedia.org/wikipedia/commons/d/d2/Varanidae_-_Varanus_macraei.jpg",
  //     id: 38,
  //     value: 100
  //   },
  // ];
  public purchasable: Animal[] = [

  ];
}
